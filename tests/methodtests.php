<?php

	use PHPUnit\Framework\TestCase;
	class DataTest extends TestCase
	{
		/**
		*@dataProvided http://gitlab/charlie6843/hw4
		*/

		public function testMethod($create, $view, $del)
		{
			$data = [
				//each items represents the method parameter
				[create],
				[view],
				[del],
				];

			return $data;
		}
}
?>